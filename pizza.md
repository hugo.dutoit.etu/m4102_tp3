| URI                      | Opération   | MIME                                                         | Requête         | Réponse                                                              |
| :----------------------- | :---------- | :---------------------------------------------               | :--             | :----------------------------------------------------                |
| /pizzas                  | GET         | <-application/json<br><-application/xml                      |                 | liste des pizzas(P2)                                           |
| /pizzas/{id}        | GET         | <-application/json<br><-application/xml                      |                 | un ingrédient (P2) ou 404                                            |
| /pizzas/{id}/name   | GET         | <-text/plain                                                 |                 | le nom de l'ingrédient ou 404      
| /ingredients             | POST        | <-/->application/json<br>->application/x-www-form-urlencoded | Pizza(P1) | Nouvel Pizza(P2) <br>409 si l'ingrédient existe déjà (même nom) |                                  |
| /pizzas/{id}        | DELETE      |                                                              |                 |                                                                      |


(P2)

    {
      "id": "098906g8-sd85-sfef-rf0c-149fdfsd906",
      "name": "Napolitan"
      "ingredient":"[{'name':'tomat'},{'name':'anchoie'}]"
    }


(P1)

    { "name": "Napolitant" }