## Développement d'une ressource *commande*

### API et représentation des données


| URI                       | Opération   | MIME                                                         | Requête         | Réponse                                                       |
| :----------------------- | :---------- | :---------------------------------------------               | :--             | :----------------------------------------------------         |
| /commande                   | GET         | <-application/json<br><-application/xml                      |                 | Liste des commandes                                          |
| /commande/{id}              | GET         | <-application/json<br><-application/xml                      |                 | Une commandes ou 404                                        |                                  |
| /commande/client/{client}              | GET         | <-application/json<br><-application/xml                      |                 | Une commande en fonction du nom du client ou 404                                        |                                  |
| /commande/{id}/pizza              | GET         | <-application/json<br><-application/xml                      |                 | les pizza d une commande ou 404                                        |                                  |
| /commande                   | POST        | <-/->application/json<br>->application/x-www-form-urlencoded | commande      | Nouvelle commande <br>409 si la commande existe déjà (même nom ou meme id) |
| /commande/{id}              | DELETE      | <-application/json<br><-application/xml                      |                 | Supprime une commande                                            |


Représentation d'une commande sans l'id

    {
        "client": "Jose"
        "pizzas" : [    
            {
                "id": "098906g8-sd85-sfef-rf0c-149fdfsd906",
                "name": "Napolitan"
                "ingredient":"[{'name':'tomat'},{'name':'anchoie'}]"
            }
        ]
    }

Représentation d'une commande avec un id, un nom et une liste de pizzas

    {
      "id": "098906g8-sd85-sfef-rf0c-149fdfsd906",
      "client": "Jose"
      "pizzas" : [    
            {
                "id": "098906g8-sd85-sfef-rf0c-149fdfsd906",
                "name": "Napolitan"
                "ingredient":"[{'name':'tomat'},{'name':'anchoie'}]"
            }
        ]
    }