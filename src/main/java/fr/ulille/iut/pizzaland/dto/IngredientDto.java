package fr.ulille.iut.pizzaland.dto;

import java.util.UUID;

import fr.ulille.iut.pizzaland.beans.Ingredient;

public class IngredientDto {
    private UUID id;
    private String name;

    public IngredientDto() {
      //cause itshould be
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getId() {
        return id;
    }

    public void setName(String name) {
      this.name = name;
    }

    public String getName() {
      return name;
    }
    
    public static IngredientCreateDto toCreateDto(Ingredient ingredient) {
        IngredientCreateDto dto = new IngredientCreateDto();
        dto.setName(ingredient.getName());
            
        return dto;
      }
    	
 
    public static Ingredient fromIngredientCreateDto(IngredientCreateDto dto) {
    	Ingredient ingredient = new Ingredient();
	    ingredient.setName(dto.getName());
	
	    return ingredient;
	}

}
