package fr.ulille.iut.pizzaland.dto;

public class IngredientCreateDto {

	private String name;
	
	public IngredientCreateDto() {
		//useless
	}
		
	public void setName(String name) {
		this.name = name;
	}
 		
	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return "IngredientCreateDto [name=" + name + "]";
	}
	
	
}
