package fr.ulille.iut.pizzaland.dao;

import org.glassfish.jersey.test.JerseyTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.Arrays;

import fr.ulille.iut.pizzaland.ApiV1;
import fr.ulille.iut.pizzaland.BDDFactory;
import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dao.IngredientDao;
import jakarta.ws.rs.core.Application;
import jakarta.ws.rs.core.Response;

public class PizzaDaoTest extends JerseyTest{
    private PizzaDao dao;
    private IngredientDao Idao;
    
    
    private Pizza p = new Pizza("Calezon");
    private Pizza pWingridient = new Pizza("Calezon",Arrays.asList(new Ingredient[]{new Ingredient("tomate")}));
    @Override
    protected Application configure() {
        BDDFactory.setJdbiForTests();
        return new ApiV1();
    }
    
    @Before
    public void setEnvUp() {
      dao = BDDFactory.buildDao(PizzaDao.class);
      dao.createTableAndIngredientAssociation();
      Idao = BDDFactory.buildDao(IngredientDao.class);
      Idao.createTable();
    }

    @After
    public void tearEnvDown() throws Exception {
       dao.dropTableAndIngredientAssociation();
       Idao.dropTable();
    }
    
    @Test
    public void DaoInsertAndFindByIdTest(){
		dao.insertPizza(p);
		assertEquals(p, dao.findById(p.getId()));
    }
    
    @Test
    public void DaoInsertAndFindByNameTest() {
		dao.insertPizza(p);
		assertEquals(p, dao.findByName(p.getName()));
    }
    
    @Test
    public void DaoDeletePizzaTest() {
    	dao.insertPizza(p);
    	assertEquals(p,dao.findById(p.getId()));
    	dao.deletePizzaById(p.getId());
    	assertNull(dao.findById(p.getId()));
    }
    @Test
    public void DaoInsertAssociationTest() {
    	Idao.insert(pWingridient.getIngredients().get(0));
    	dao.insertPizza(pWingridient);
    	dao.insertAssos(pWingridient.getId(),pWingridient.getIngredients().get(0).getId());
    	assertEquals(pWingridient, dao.findFullPizzaById(pWingridient.getId()));
    }
    
    @Test
    public void DaoinsertPizzawithIngridientsTest() {
    	dao.insertPizzawithIngridients(pWingridient);
    	assertEquals(pWingridient, dao.findFullPizzaById(pWingridient.getId()));
    }
}

